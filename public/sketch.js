let fcritData;
let dataCoords;
let markers = [];
let fcritStudents = [];
let map;
let geocoder;
let p;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function preload() {
    fcritData = loadTable("FcritStudentsDetails.csv", "csv", "header");
    dataCoords = loadTable("coords.csv", "csv", "header");
}

function setup() {
    noCanvas();
    p = createP();
    console.log("starting.....");


    for (let i = 0; i < fcritData.getRowCount(); i++) {

        let entry = fcritData.getRow(i);
        let coordsEntry = dataCoords.getRow(i)["arr"];
        let rollNo = entry.arr[0];
        let name = entry.arr[1];
        let dob = entry.arr[2];
        let gender = entry.arr[3];
        let currAddress = entry.arr[4];
        let permAddress = entry.arr[5];
        let mobileNo = entry.arr[6];
        let email = entry.arr[7];
        let coords = { lat: coordsEntry[0], lng: coordsEntry[1] };
        let f = new FCRITStudent(rollNo, name, dob, gender, currAddress, permAddress, mobileNo, email, coords);
        fcritStudents.push(f);
    }

    map = L.map('map', {
        center: [19.141116, 72.955158],
        zoom: 10
    });
    const attribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
    const tileUrl = 'https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png';
    const tiles = L.tileLayer(tileUrl, { attribution });
    tiles.addTo(map);


    for (let f = 0; f < fcritStudents.length; f++) {
        fcs = fcritStudents[f];
        coords = fcs.coords;
        let latlng = L.latLng(coords.lat, coords.lng);

        let opts = {
            title: `${fcs.name}:${fcs.rollNo}`,
            riseOnHover: true,
            riseOffset: 300
        }
        let layer = L.marker(latlng, opts).addTo(map);
        layer.addTo(map);
        layer.data = fcs;
        markers.push(layer);
        //if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        layer.on("click", (e) => {
            console.log(e);
            let td = e.target.data;
            L.marker(e.latlng).addTo(map).bindPopup(`${td.name}:${td.rollNo}`).openPopup();

            // L.marker(e.latlng).addTo(map).bindPopup(`<a href="profile/${td.rollNo}/">${td.name}:${td.rollNo}</a>`).openPopup();
        });
        //}
        // else {
        //     console.log("not mobile..");
        // }

    }

}