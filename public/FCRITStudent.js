class FCRITStudent {
    constructor(rollNo, name, dob, gender, currAddress, permAddress, mobileNo, email, coords) {
        this.rollNo = rollNo;
        this.name = name;
        this.dob = dob;
        this.gender = gender;
        this.currAddress = currAddress;
        this.permAddress = permAddress;
        this.mobileNo = mobileNo;
        this.email = email;
        this.coords = coords;
    }
}